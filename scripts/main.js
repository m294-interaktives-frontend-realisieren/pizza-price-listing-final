const pizzaInfos = [
  ['Margherita', 14, 16, 25],
  ['Funghi', 17.5, 20, 31.5],
  ['Stromboli', 15.5, 18, 28],
  ['Quattro formaggi', 19.5, 22.5, 35],
  ['Auguri', 23.5, 27, 42.5],
];

function getPrice() {
  // Index selektierter Pizza ermitteln
  const idxPizza = document.getElementById('select-pizza').selectedIndex;
  // Index selektierter Grösse ermitteln
  const idxSize = document.getElementById('select-size').selectedIndex;
  // Preis ausgeben
  const elOutput = document.getElementById('output');
  let sizeText = 'kleinen';
  if (idxSize === 1) {
    sizeText = 'normalen';
  } else if (idxSize === 2) {
    sizeText = 'grossen';
  }
  const pizzaInfo = pizzaInfos[idxPizza];
  elOutput.textContent = `Preise der ${sizeText} Pizza ${pizzaInfo[0]} ist ${
    pizzaInfo[1 + idxSize]
  } Fr.`;
}

function createDropdowns() {
  function createDropdown(elSelect, options) {
    for (let i = 0; i < options.length; i++) {
      const option = options[i];
      const elOp = document.createElement('option');
      elOp.textContent = option;
      elOp.setAttribute('value', i);
      elSelect.appendChild(elOp);
    }
  }
  // Dropdown für Pizza Auszahl erstellen
  const pizzaNames = [];
  for (let i = 0; i < pizzaInfos.length; i++) {
    pizzaNames.push('Pizza ' + pizzaInfos[i][0]);
  }
  const elSelectPizza = document.getElementById('select-pizza');
  createDropdown(elSelectPizza, pizzaNames);
  // Dropdown für Grösse erstellen
  const elSelectSize = document.getElementById('select-size');
  createDropdown(elSelectSize, [
    'klein (24cm)',
    'normal (30cm)',
    'gross(40cm)',
  ]);
}

createDropdowns();
